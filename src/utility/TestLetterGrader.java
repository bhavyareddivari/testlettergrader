package utility;

import java.io.IOException;

/**
 * This class calculates letter grade for each student and display grades in output file
 * and also displays average, maximum and minimum of all students in each quiz, midterms and finals 
 * 
 * @author Bhavya
 *
 */
public class TestLetterGrader {
	
	//Main method
	public static void main(String[] args) throws IOException  {
		//checking if there are two valid arguments
		if(args.length==2){
			//creating object
			LetterGrader letterGrader = new LetterGrader(args[0], args[1]);
			letterGrader.readScore(); 
			letterGrader.calcLetterGrade(); 
			letterGrader.printGrade(); 
			letterGrader.displayAverages(); 
			letterGrader.doCleanup();		
		} else{
			System.out.println("Add arguments");
			System.exit(0);
		}     
	
	}

}
