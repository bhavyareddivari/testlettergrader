package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*
 * This class calculates the letter grade for all the students. It reads the
 * data for all students from input file, calculates grade and prints the grade
 * for each student in a sorted manner in output file. It also calculates
 * the average, minimum and maximum for each quiz and project and display it
 * in console.
 */
public class LetterGrader {
	// declaring variables
	private String inputFile;
	private String outputFile;
	private FileWriter writer;
	private BufferedReader buffer;
    Scanner sc= new Scanner(System.in);

	// declaring arraylist to store information
	List<Integer> scoreArray = new ArrayList<Integer>();
	List<String> namesArray = new ArrayList<String>();
	List<String> gradeArray = new ArrayList<String>();
	List<String> quiz1 = new ArrayList<String>();
	List<String> quiz2 = new ArrayList<String>();
	List<String> quiz3 = new ArrayList<String>();
	List<String> quiz4 = new ArrayList<String>();
	List<String> mid1 = new ArrayList<String>();
	List<String> mid2 = new ArrayList<String>();
	List<String> finalProject = new ArrayList<String>();
	List<Double> sum = new ArrayList<Double>();
	List<Double> avg = new ArrayList<Double>();

	/**
	 * Constructor
	 * 
	 * @param in
	 * @param out
	 */
	public LetterGrader(String inputFile, String outputFile) {
		// Initializing variables
		this.inputFile = inputFile;
		this.outputFile = outputFile;
	}

	/**
	 * Method to read scores of students from given input file
	 * 
	 * @throws IOException
	 */
	public void readScore() throws IOException {
		// declaring variable line
		String line = null;
		// opening file
		File file = new File(inputFile);
		buffer = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		while ((line = buffer.readLine()) != null) {
			lines.add(line);
		}
		Collections.sort(lines);
		// writing sorted data to input file
		FileWriter writer = new FileWriter(inputFile);
		for (String str : lines) {
			writer.write(str + "\r\n");
		}
		writer.close();
		buffer = new BufferedReader(new FileReader(file));
		// Reading each line in the file until EOF
		while ((line = buffer.readLine()) != null) {
			// removing spaces
			line = line.trim();
			double finalScore = 0;
			// split each line into words with delimiter ","
			String[] fields = line.split(",");
			// creating numbers array to store marks
			int[] numbers = new int[fields.length];
			// marks of each student is converted from string to integer
			for (int i = 0; i < fields.length - 1; i++) {
				fields[i + 1] = fields[i + 1].trim();
				try {
					// casting from string to integer
					numbers[i] = Integer.parseInt(fields[i + 1]);
					// System.out.println(numbers[i]);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			// code to calculate the total score of each student to calculate
			// grade
			for (int i = 0; i < numbers.length - 1; i++) {
				if (i < 4)
					finalScore += numbers[i] * 0.10;
				else if (i == 4)
					finalScore += numbers[i] * 0.20;
				else if (i == 5)
					finalScore += numbers[i] * 0.15;
				else if (i == 6)
					finalScore += numbers[i] * 0.25;
			}
			// adding marks of each student in different lists
			scoreArray.add((int) finalScore);
			namesArray.add(fields[0]);
			quiz1.add(fields[1]);
			quiz2.add(fields[2]);
			quiz3.add(fields[3]);
			quiz4.add(fields[4]);
			mid1.add(fields[5]);
			mid2.add(fields[6]);
			finalProject.add(fields[7]);
		}
	}

	/**
	 * method to determine letter grade of each student and stores grade in
	 * arraylist
	 */
	public void calcLetterGrade() {
		//for loop to calculate grade for all students
		for (int i = 0; i < scoreArray.size(); i++) {
			if (scoreArray.get(i) >= 90)
				gradeArray.add("A");
			else if (scoreArray.get(i) >= 80 && scoreArray.get(i) <= 89)
				gradeArray.add("B");
			else if (scoreArray.get(i) >= 70 && scoreArray.get(i) <= 79)
				gradeArray.add("C");
			else if (scoreArray.get(i) >= 60 && scoreArray.get(i) <= 69)
				gradeArray.add("D");
			else
				gradeArray.add("F");
		}
	}

	/**
	 * Method to print name of the student and grade obtained in an output file
	 * 
	 * @throws IOException
	 */
	public void printGrade() throws IOException {
		// opening output file to write information
		writer = new FileWriter(outputFile, false);
		// printing to output file
		writer.write("Letter grade for " + namesArray.size() + " students given in " + inputFile + " file is:" + "\n\n");
		// printing names and grade to output file
		for (int i = 0; i < scoreArray.size(); i++) {
			int numTabs = (35 - (namesArray.get(i).length() + 5)) / 5;
			writer.write(namesArray.get(i) + ":");
			for (int j = 0; j < numTabs && j < 4; j++) {
				writer.write("\t");
			}
			writer.write(gradeArray.get(i) + "\n");
		}
		System.out.println("\nLetter grade has been calculated for students listed in input file "
				+ inputFile + " and written to output file "
			    + outputFile + "\n");
	}

	/**
	 * Method to display average,maximum and minimum of all students in console
	 */
	public void displayAverages() {
		// defining and initializing variables for each quiz,midterms and final
		double sumQ1 = 0, sumQ2 = 0, sumQ3 = 0;
		double sumQ4 = 0, sumMid1 = 0, sumMid2 = 0, sumFinal = 0;
		int maxQ1 = 0, maxQ2 = 0, maxQ3 = 0, maxQ4 = 0;
		int maxMid1 = 0, maxMid2 = 0, maxFinal = 0;
		int minQ1 = 0, minQ2 = 0, minQ3 = 0, minQ4 = 0;
		int minMid1 = 0, minMid2 = 0, minFinal = 0;

		// Writing to console
		System.out.println("\nHere is the class averages:");
		System.out.println("\n\t\tQ1\tQ2\tQ3\tQ4\tMidI\tMidII\tFinal");

		// calculating average, maximum and minimum for quiz1
		for (int i = 0; i < namesArray.size(); i++) {
			int q1 = (int) Double.parseDouble(quiz1.get(i));
			if (i == 0)
				minQ1 = maxQ1 = q1;
			sumQ1 += q1;
			if (q1 > maxQ1)
				maxQ1 = q1;
			if (minQ1 > q1)
				minQ1 = q1;
		}
		// calculating average, maximum and minimum for quiz2
		for (int i = 0; i < namesArray.size(); i++) {
			int q2 = (int) Double.parseDouble(quiz2.get(i));
			if (i == 0)
				minQ2 = maxQ2 = q2;
			sumQ2 += q2;
			if (q2 > maxQ2)
				maxQ2 = q2;
			if (minQ2 > q2)
				minQ2 = q2;
		}
		// calculating average, maximum and minimum for quiz3
		for (int i = 0; i < namesArray.size(); i++) {
			int q3 = (int) Double.parseDouble(quiz3.get(i));
			if (i == 0)
				minQ3 = maxQ3 = q3;
			sumQ3 += q3;
			if (q3 > maxQ3)
				maxQ3 = q3;
			if (minQ3 > q3)
				minQ3 = q3;
		}
		// calculating average, maximum and minimum for quiz4
		for (int i = 0; i < namesArray.size(); i++) {
			int q4 = (int) Double.parseDouble(quiz4.get(i));
			if (i == 0)
				minQ4 = maxQ4 = q4;
			sumQ4 += q4;
			if (q4 > maxQ4)
				maxQ4 = q4;
			if (minQ4 > q4)
				minQ4 = q4;
		}
		// calculating average, maximum and minimum for mid1
		for (int i = 0; i < namesArray.size(); i++) {
			int m1 = (int) Double.parseDouble(mid1.get(i));
			if (i == 0)
				minMid1 = maxMid1 = m1;
			sumMid1 += m1;
			if (m1 > maxMid1)
				maxMid1 = m1;
			if (minMid1 > m1)
				minMid1 = m1;
		}
		// calculating average, maximum and minimum for mid2
		for (int i = 0; i < namesArray.size(); i++) {
			int m2 = (int) Double.parseDouble(mid2.get(i));
			if (i == 0)
				minMid2 = maxMid2 = m2;
			sumMid2 += m2;
			if (m2 > maxMid2)
				maxMid2 = m2;
			if (minMid2 > m2)
				minMid2 = m2;
		}
		// calculating average, maximum and minimum for final
		for (int i = 0; i < namesArray.size(); i++) {
			int finals = (int) Double.parseDouble(finalProject.get(i));
			if (i == 0)
				minFinal = maxFinal = finals;
			sumFinal += finals;
			if (finals > maxFinal)
				maxFinal = finals;
			if (minFinal > finals)
				minFinal = finals;
		}
		// writes to console
		System.out.printf("Average:\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f", sumQ1 / namesArray.size(),
				sumQ2 / namesArray.size(), sumQ3 / namesArray.size(), sumQ4 / namesArray.size(),
				sumMid1 / namesArray.size(), sumMid2 / namesArray.size(), sumFinal / namesArray.size());
		System.out.println("\nMinimum:\t" + minQ1 + "\t" + minQ2 + "\t" + minQ3 + "\t" + minQ4 + "\t" + minMid1 + "\t"
				+ minMid2 + "\t" + minFinal);
		System.out.println("Maximum:\t" + maxQ1 + "\t" + maxQ2 + "\t" + maxQ3 + "\t" + maxQ4 + "\t" + maxMid1 + "\t"
				+ maxMid2 + "\t" + maxFinal);
		System.out.println("\nPress Enter to continue..."); // ask user to continue or not.
        sc.nextLine();
	}

	/**
	 * Method to close input and output files
	 */
	public void doCleanup() {
		try {
			buffer.close();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
